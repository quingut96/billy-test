<?php

# challenge 1
function getPiDecimal(){
# your code here
	
	//solucion 1:
	//obtener el decimal en cualquier posicion de cualquier numero
	
	$numero = pi(); // cualquier numero
	$posicion = 10; // posicion que desea obtener
	
	$explode = explode('.',$numero); //dividimos el numero en 2 partes
									//explode[0] es el numero entero
									//explode[1] es el decimal
	
	$longitudexplode1 = strlen($explode[1]); //obtenemos la longitud de cadena del decimal
	
	return substr($explode[1],$posicion-1,$posicion-$longitudexplode1); // Mediante esta funcion obtenemos el valor deseado
	
	//solucion 2:
	//$pi = pi(); //3.1415926535898;
	//return substr($pi,11,-3);
	//return 5;
}

# challenge 2
function getSumEvens($a=[1,2,3,4.5,6]){
# your code here
	
	foreach($a as &$valor){ 		//cogemos el array $a como $valor
		if(is_float($valor)){		//filtra si la $valor es float
			$valor = floor($valor);	//si es float con floor lo convierte en entero
			}
		else if($valor % 2 != 0){ // filtra si $valor es par
			$valor = NULL;			// si no es par lo volvera nulo e imposible de sumar
			}
		}
		
	return array_sum($a); // suma los valores convertidos
	//return 12;
}

# challenge 3
function getOrderedVowels($s="just a testing"){
# your code here
	//hacemos un array excluendo aeiou
	$array = ['b','c','d','f','g','h','j','k','m','n','p','q','r','s','t','v','w','x','y','z',' ']; 
	// con str_replace replanzamos los caracteres(array) con '' .
	$testear = str_replace($array,'',$s);
	//devuelve las vocales
	return $testear;
	
	//return "uaei";
}

# challenge 4
# obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){
# your code here
	//optiene info de la pagina
	$pagina = file_get_contents('https://jsonplaceholder.typicode.com/users');
	// esta info esta como json, asi que la transformamos con json_decode a variable php
	$pagina = json_decode($pagina,true);
	//la variable $pagina tiene arrays multidimensionales, $pagina[0] obtiene el primer valor
	//este primer valor contiene otro array con clave de id;
	return $pagina[0]['id'];
	
	//return 1;
}
# DONT EDIT
echo "Running: \n";
echo "challenge 1: ".((getPiDecimal()==5)? "pass" : "fail") . "\n" ;
echo "challenge 2: ".((getSumEvens()==12)? "pass" : "fail" ) . "\n" ;
echo "challenge 3: ".((getOrderedVowels()=="uaei")? "pass" : "fail") . "\n" ;
echo "challenge 4: ".((getFirstId()==1)? "pass" : "fail" ).		 "\n" ;
